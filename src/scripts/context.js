inject(function () {
	let panels = {};
	let rinfo = /\(\d{1,3}\|\d{1,3}\)\s\w\d{1,2}$/;

	let $village;
	let data;

	let isSelectedVillage;
	let isHeaderElement;

	jQuery('span.village_anchor, #menu_row2_village').contextmenu(function (event) {
		event.preventDefault();

		$village = jQuery(this);
		data = $village.data();

		isSelectedVillage = !('id' in data) || game_data.village.id === data.id;
		isHeaderElement = $village.attr('id') === 'menu_row2_village';

		data.id = data.id || game_data.village.id;
		data.player = data.player || game_data.player.id;

		let key = data.id + (isHeaderElement ? 'header' : '');
		let panel;

		if (key in panels) {
			panel = panels[key];
		} else {
			panels[key] = panel = e.panel({
				title: '<span class="icon header village"></span>',
				relative: $village,
				addClass: 'e-villageContext',
				reposition: true
			});

			if (data.player == game_data.player.id) {
				panel.append(
					`<section>
						<p>
							<a class="e-rename">Renomear aldeia</a>
							<input class="e-renameInput" title='"Enter" para salvar, "Esc" para cancelar.' type="text"/>
						</p>
						<p><a class="e-showCommands">Exibir comandos</a></p>
					</section>`
				);

				setVillageRenamer(panel.elements.rename, panel.elements.renameInput, data);
			}

			// // FOR THE FUTURE
			// panel.append(
			// 	`<section>
			// 		<p><a class="e-highlight" href="#">Marcar aldeia no mapa</a></p>
			// 	</section>`
			// );

			e.tooltip(panel.$.find('[title]'));
		}

		panel.show();

		return false;
	});

	function setVillageRenamer ($container, $input) {
		let name = isHeaderElement
			? $village.text().trim()
			: $village.text().replace(rinfo, '').trim();

		$input.hide().val(name)

		$container.click(function () {
			$container.hide();
			$input.show();
			$input.focus();
		});

		$input.keyup(function (event) {
			if (event.keyCode === 27) {
				$container.show();
				$input.hide();
			} else if (event.keyCode === 13) {
				TribalWars.showLoadingIndicator();

				e.renameVillage(data.id, this.value, (error) => {
					TribalWars.hideLoadingIndicator();

					if (error) {
						UI.SuccessMessage(error);
					} else {
						if (isSelectedVillage) {
							jQuery('#menu_row2_village a')
								.contents()
								.filter((i, elem) => elem.nodeType === 3)
								.replaceWith(this.value);
						}

						if (!isHeaderElement) {
							let info = $village.text().match(rinfo);

							$village.find('a')
								.contents()
								.filter((i, elem) => elem.nodeType === 3)
								.replaceWith(`${this.value} ${info}`);
						}

						UI.SuccessMessage('Aldeia renomeada!');
					}

					$container.show();
					$input.hide();
				});
			}
		});
	}
});

function inject (fn) {
	var script = document.createElement('script');
	script.appendChild(document.createTextNode('!' + fn.toString() + '();'));
	document.body.appendChild(script);
	script.remove();
}