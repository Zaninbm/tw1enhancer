inject(function () {
	let buildingPoints = {
		main: [0, 10, 2, 2, 3, 4, 4, 5, 6, 7, 9, 10, 12, 15, 18, 21, 26, 31, 37, 44, 53, 64, 77, 92, 110, 133, 159, 191, 229, 274, 330],
		barracks: [0, 16, 3, 4, 5, 5, 7, 8, 9, 12, 14, 16, 20, 24, 28, 34, 42, 49, 59, 71, 85, 102, 123, 147, 177, 212],
		stable: [0, 20, 4, 5, 6, 6, 9, 10, 12, 14, 17, 21, 25, 29, 36, 43, 51, 62, 74, 88, 107],
		garage: [0, 24, 5, 6, 6, 9, 10, 12, 14, 17, 21, 25, 29, 36, 43, 51],
		church: [0, 10, 2, 2],
		church_f: [0, 10],
		snob: [0, 512],
		smith: [0, 19, 4, 4, 6, 6, 8, 10, 11, 14, 16, 20, 23, 28, 34, 41, 49, 58, 71, 84, 101],
		place: [0, 0],
		statue: [0, 24],
		market: [0, 10, 2, 2, 3, 4, 4, 5, 6, 7, 9, 10, 12, 15, 18, 21, 26, 31, 37, 44, 53, 64, 77, 92, 110, 133],
		wood: [0, 6, 1, 2, 1, 2, 3, 3, 3, 5, 5, 6, 8, 8, 11, 13, 15, 19, 22, 27, 32, 38, 46, 55, 66, 80, 95, 115, 137, 165, 198],
		stone: [0, 6, 1, 2, 1, 2, 3, 3, 3, 5, 5, 6, 8, 8, 11, 13, 15, 19, 22, 27, 32, 38, 46, 55, 66, 80, 95, 115, 137, 165, 198],
		iron: [0, 6, 1, 2, 1, 2, 3, 3, 3, 5, 5, 6, 8, 8, 11, 13, 15, 19, 22, 27, 32, 38, 46, 55, 66, 80, 95, 115, 137, 165, 198],
		farm: [0, 5, 1, 1, 2, 1, 2, 3, 3, 3, 5, 5, 6, 8, 8, 11, 13, 15, 19, 22, 27, 32, 38, 46, 55, 66, 80, 95, 115, 137, 165],
		storage: [0, 6, 1, 2, 1, 2, 3, 3, 3, 5, 5, 6, 8, 8, 11, 13, 15, 19, 22, 27, 32, 38, 46, 55, 66, 80, 95, 115, 137, 165, 198],
		hide: [0, 5, 1, 1, 2, 1, 2, 3, 3, 3, 5],
		wall: [0, 8, 2, 2, 2, 3, 3, 4, 5, 5, 7, 9, 9, 12, 15, 17, 20, 25, 29, 36, 43]
		// watchtower: []
	};

	function getMode () {
		let mode = $('.modemenu td:first').hasClass('selected') ? 'build' : 'destroy';

		if (!$('.modemenu').size()) {
			mode = 'build';
		}

		return mode;
	}

	function getBuildings () {
		let $buildings = $('#building_wrapper tr:not(:first)');

		if (getMode() === 'build') {
			$buildings = $buildings.has('[id]');
		}

		return $buildings;
	}

	function getLevel (text) {
		return /\d+/.test(text) ? e.getNum(text) : 0;
	}

	function nextPointLevel () {
		let mode = getMode();
		let $buildings = getBuildings();

		$buildings.each(function () {
			if ($('td', this).size() === 2) {
				return;
			}

			let building;
			let level;
			let nextLevel;
			let $span;

			if (mode === 'build') {
				building = this.id.replace('main_buildrow_', '');
				level = parseInt(game_data.village.buildings[building], 10);
				nextLevel = level + $(`#buildqueue tr.buildorder_${building}`).size() + 1;

				$span = $('td:first span', this);

				if (building in buildingPoints) {
					$span.append(` (${nextLevel}: <span class="e-green">+${buildingPoints[building][nextLevel]}</span>)`);
				}
			} else {
				building = $('a:first', this).attr('href').match(/screen=(\w+)/)[1];
				level = parseInt(game_data.village.buildings[building], 10);

				if (building in buildingPoints) {
					$('td:first', this).append(` <span class="e-red">-${buildingPoints[building][level]}</span>`);
				}
			}
		});
	}

	function nextPointLevelOrders () {
		$('#buildqueue tr[class*=buildorder]').each(function () {
			let building = this.className.match(/buildorder_(\w+)$/)[1];
			let text = $('td:first', this).text();
			let level;
			let destroy = false;

			if (/\d/.test(text)) {
				level = e.getNum(text);
			} else {
				destroy = true;
				level = game_data.village.buildings[building]--;
			}

			let points = buildingPoints[building][level];

			$('td:first', this).append(destroy
				? `(<span class="e-red">-${points}</span>)`
				: `(<span class="e-green">+${points}</span>)`
			);
		});
	}

	nextPointLevelOrders();
	nextPointLevel();

	$(document).on('DOMNodeRemoved', '#buildqueue_wrap', function () {
		setTimeout(function () {
			nextPointLevelOrders();
			nextPointLevel();
		}, 0);
	});
});

function inject (fn) {
	var script = document.createElement('script');
	script.appendChild(document.createTextNode('!' + fn.toString() + '();'));
	document.body.appendChild(script);
	script.remove();
}