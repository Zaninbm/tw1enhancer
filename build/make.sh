dest=dist/chromium

rm -rf $dest
mkdir -p $dest

cp -R src					$dest/src
cp -R platform/chromium/*	$dest
cp -R LICENSE.md 			$dest
cp -R README.md 			$dest