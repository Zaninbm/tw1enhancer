inject(function () {
	let id = location.search.match(/id=(\d+)/);
	let type = location.search.includes('ally') ? 'tribe' : 'player';

	id = !id
		? game_data.player.id
		: id[1];

	let ui = (function () {
		let stats = e.bar({
			title: e.lang.profileStats.stats
		});

		let panel = e.panel({
			title: e.lang.profileStats.playerStats,
			relative: stats.$,
			addClass: 'e-stats',
			width: 360
		});

		stats.click(function () {
			panel.toggle();
			return false;
		});

		panel.append(
			`<section>
				<p><b>${e.lang.profileStats.daily}</b></p>
				<table>
					<tr><td>${e.lang.profileStats.oda}:</td><td><b><span class="e-att"></span></b></td><td><span class="e-attr"></span>º</td><td><i><span class="e-attd"></span></i></td></tr>
					<tr><td>${e.lang.profileStats.odd}:</td><td><b><span class="e-def"></span></b></td><td><span class="e-defr"></span>º</td><td><i><span class="e-defd"></span></i></td></tr>
					<tr><td>${e.lang.profileStats.ods}:</td><td><b><span class="e-sup"></span></b></td><td><span class="e-supr"></span>º</td><td><i><span class="e-supd"></span></i></td></tr>
					<tr><td>${e.lang.profileStats.loot}:</td><td><b><span class="e-loot"></span></b></td><td><span class="e-lootr"></span>º</td><td><i><span class="e-lootd"></span></i></td></tr>
					<tr><td>${e.lang.profileStats.vil}:</td><td><b><span class="e-vil"></span></b></td><td><span class="e-vilr"></span>º</td><td><i><span class="e-vild"></span></i></td></tr>
					<tr><td>${e.lang.profileStats.conquer}:</td><td><b><span class="e-conquer"></span></b></td><td><span class="e-conquerr"></span>º</td><td><i><span class="e-conquerd"></span></i></td></tr>
				</table>
			</section>
			<section>
				<p><b>${e.lang.profileStats.graphs}</b></p>
				<p class="e-selectGraph">
					<a class="e-points" style="font-weight:bold" name="p_player">${e.lang.general.points}</a> -
					<a class="e-oda" name="oda_player">ODA</a> -
					<a class="e-odd" name="odd_player">ODD</a> -
					<a class="e-ods" name="ods_player">ODS</a> -
					<a class="e-villages" name="villages_player">${e.lang.general.villages}</a>
				</p>
				<p><img class="e-twmGraph"/></p>
			</section>`
		);

		let url = `http://${game_data.world}.tribalwarsmap.com/${game_data.market}`

		panel.elements.twmGraph.attr('src', `${url}/graph/p_player/${id}`);

		panel.elements.selectGraph.find('a').click(function () {
			panel.elements.selectGraph.find('a').css('font-weight', 'normal');
			this.style.fontWeight = 'bold';

			panel.elements.twmGraph.attr('src', `${url}/graph/${this.name}/${id}`);
		});

		getOnedayData('kill_att', function (ranking, amount, when) {
			panel.elements.att.text(amount);
			panel.elements.attr.text(ranking);
			panel.elements.attd.text(when);
		});

		getOnedayData('kill_def', function (ranking, amount, when) {
			panel.elements.def.text(amount);
			panel.elements.defr.text(ranking);
			panel.elements.defd.text(when);
		});

		getOnedayData('kill_sup', function (ranking, amount, when) {
			panel.elements.sup.text(amount);
			panel.elements.supr.text(ranking);
			panel.elements.supd.text(when);
		});

		getOnedayData('loot_res', function (ranking, amount, when) {
			panel.elements.loot.text(amount);
			panel.elements.lootr.text(ranking);
			panel.elements.lootd.text(when);
		});

		getOnedayData('loot_vil', function (ranking, amount, when) {
			panel.elements.vil.text(amount);
			panel.elements.vilr.text(ranking);
			panel.elements.vild.text(when);
		});

		getOnedayData('conquer', function (ranking, amount, when) {
			panel.elements.conquer.text(amount);
			panel.elements.conquerr.text(ranking);
			panel.elements.conquerd.text(when);
		});
	})();

	function getOnedayData (type, callback) {
		let url = e.url({
			screen: 'ranking',
			mode: 'in_a_day',
			type: type,
			name: $('h2').text().trim(),
			_partial: ''
		});

		$.get(url, function (data) {
			let $tr = $(data.content).find('#in_a_day_ranking_table tr[class]');

			let ranking = $tr.find('td:first').text();
			let amount = $tr.find('td:eq(3)').text();
			let when = $tr.find('td:eq(4)').text();

			callback(ranking, amount, when);
		}, 'json');
	}
});

function inject (fn) {
	var script = document.createElement('script');
	script.appendChild(document.createTextNode('!' + fn.toString() + '();'));
	document.body.appendChild(script);
	script.remove();
}